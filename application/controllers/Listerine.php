<?php
class Listerine extends CI_Controller{

   
	function __construct()
	{
	  parent::__construct();
	  $this->load->model('model_listerine');
          $this->load->model('model_sample');
          $this->load->model('model_quiz');
          $this->load->model('model_userloginfb');
          $this->load->model('model_userlogintw');
          $this->load->library('facebook');
          
          // Loading TwitterOauth library. Delete this line if you choose autoload method.
            $this->load->library('twitteroauth');
          // Loading twitter configuration.
            $this->config->load('twitter');
     
          
        }
	
	
	function index()
	{
	 
          
          
          
          //$data['head'] = $this->load->view('head');          
          //$this->load->view('home',$data);
         
	
	}
        
        public function home()
	{
             $this->load->helper('share');
         //buat objek array buat menampung data user login nantinya
		$data['user'] = array();

		if ($this->facebook->logged_in()) //cek jika sudah ada session login active facebook sebelumnya
		{
			$user = $this->facebook->user(); //ambil data user dari fungsi ini isi ke variabel $user

			if ($user['code'] === 200) //The HTTP status code "200" means the request is successfully handled by the Server, and "200 OK" is standard HTTP status code
			{
				unset($user['data']['permissions']); //di unset dulu session yang ada
				$data['user'] = $user['data'];    //baru di isi dengan data yang baru ke objek array yang tadi baru dibuat
                                $dataU = array(
                                    "id_fb" =>  $data['user']['id'],
                                    "nama" =>   $data['user']['name'],
                                    "email" => $data['user']['email']
                                );
                                
                                  $this->model_userloginfb->simpan($dataU);
                                
                        }
               
                      
                        

		}
                $data['head'] = $this->load->view('head');  
		$this->load->view('home', $data); //kalo tidak ada session yang aktif masuk kesini
	}
	
	
        
           // Logout from facebook
         public function logout()
	{
	     $this->facebook->destroy_session();
             redirect('listerine/home');
	}
        
        
        
        
        
        function simpanPost()
	{
                $this->form_validation->set_rules('msg','message','required|min_length[5]|max_length[230]');
		$data['message'] = $this->input->post('msg',true); //memakai security helper
            
                 if ($this->form_validation->run() == FALSE)
                {
                     //nantinya muncul pop up disini juga kalo salah
                      $this->session->set_flashdata('notif','kolom harus diisi terlebih dahulu !, silahkan diisi');
                       redirect('listerine');
                }
                else
                {
                    //nantinya muncul pop up disini juga kalo benar
                        // $this->model_listerine->setData($data);
                       // if (setData($data)){ 
                         $this->model_listerine->simpan($data);  
                         redirect('listerine');
                       // }
                  // redirect('listerine');
                }
	
	}
	
	function tambahSample()
	{
		
                $config = array(
                    array(
                          'field' =>'nama_lengkap',
                          'label' =>'nama',
                           'rules' =>'required|min_length[5]|max_length[27]'
                         
                        ),
                    array(
                          'field' =>'alamat',
                          'label' =>'alamat',
                          'rules' =>'required|min_length[10]|max_length[110]'
                         ),
                    array(
                          'field' =>'kota',
                          'label' =>'kota',
                          'rules' =>'required'
                         ),
                    array(
                          'field' =>'no_telepon',
                          'label' =>'no telepon',
                          'rules' =>'required|integer|min_length[7]|max_length[15]'
                         )
//                    array(
//                          'field' =>'konfirmasi',
//                          'label' =>'konfirmasi',
//                          
//                         )
//                    array(
//                          'field' =>'ceklist',
//                          'label' =>'ceklist'
//
//                         )              
                );
                $this->form_validation->set_rules($config);
                $data = $this->input->post(array('nama_lengkap','alamat','kota','no_telepon','konfirmasi','ceklist'),true);
		
                if($this->form_validation->run() == FALSE){
                    //nantinya muncul pop up disini juga kalo salah
                    // $this->session->set_flashdata('notifModal','input yang anda masukan belum benar!, silahkan diisi kembali');
                    //redirect('listerine');
                    echo('no');
                }
                else
                {
                    echo('yes');
                //nantinya muncul pop up disini juga kalo benar
                $this->model_sample->simpan($data);
		//redirect('listerine');
                }
	}
        
        
         
         //fungsi untuk quiz
         public function quiz()
         {
           //config rule validasi
             $config = array(
//                              array(
//                                     'field' =>'id_Userfb',
//                                     'label' =>'id_Userfb',
//                                     
//                                     ),
                             array(
                                     'field' =>'nama',
                                     'label' =>'nama',
                                    
                                     ),
                             array(
                                      'field' =>'kontak',
                                      'label' =>'kontak',
                                      'rules' =>'required'
                                  ),
                             array(
                                      'field' =>'alamatP',
                                      'label' =>'alamat',
                                      'rules' =>'required'
                                  ),
                        
                                array(
                                     'field' =>'jawaban',
                                     'label' =>'jawaban',
                                     'rules' =>'required|min_length[5]|max_length[500]'
                                     )
                         );
                $this->form_validation->set_rules($config);
                $data = $this->input->post(array('id_fb','nama','kontak','alamatP','jawaban'),true);
              
                if($this->form_validation->run() == FALSE ){
                        echo('no');
                   
                }
                elseif(! $this->facebook->logged_in()){
                         echo('belum login');
                        
                 }
                else //sudah login kesini
                {
    
                    echo ('yes');
                    $this->model_quiz->simpan($data);
               
                // redirect('listerine');
           
                 //tampilkan pop up pesan harus login dulu 
                //$this->session->set_flashdata('notifSubmit','anda belum melakukan login silahkan login terlebih dahulu di halaman utama listerine');
                //redirect('listerine');
                
              }
             
             
         }
         
         /**begin twitter code
	 * Here comes authentication twitter process begin.
	 * @access	public
	 * @return	void
	 */
	public function auth()
	{       //cek user logged in or not
            	if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// If user already logged in
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			// If user in process of authentication
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			// Unknown user
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}
                
                
                //begin create auth
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// User is already authenticated. Add your user notification code here.
			//redirect('listerine/home');
                       
		}
		else
		{
			// Making a request for request_token
			$request_token = $this->connection->getRequestToken(OAUTH_CALLBACK);

			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);
			
			if($this->connection->http_code == 200)
			{
				$url = $this->connection->getAuthorizeURL($request_token);
				redirect($url); //ke web twitter
                                $this->getTwitterData();
                              
//                                $data['head'] = $this->load->view('head');  
//                                $data['url'] = $url;
//                                $this->load->view('home', $data);
			}
			else
			{
				// An error occured. Make sure to put your error notification code here.
				//redirect(base_url('/'));
			}
		}
	}
        
        public function callback()
	{
		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->reset_session();
			redirect(base_url('/twitter/auth'));
		}
		else
		{
			$access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
		
			if ($this->connection->http_code == 200)
			{
				$this->session->set_userdata('access_token', $access_token['oauth_token']);
				$this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
				$this->session->set_userdata('twitter_user_id', $access_token['user_id']);
				$this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);

				$this->session->unset_userdata('request_token');
				$this->session->unset_userdata('request_token_secret');
				
				redirect('Listerine/home');
			}
			else
			{
				// An error occured. Add your notification code here.
				redirect(base_url('/'));
			}
		}
	}
        
  
   
     /**
	 * Reset session data
	 * @access	private
	 * @return	void
	 */
	private function reset_session()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');
                $data['head'] = $this->load->view('head');  
		$this->load->view('home', $data);
	}
        
        
//    function getTwitterData()
//    {
//    //echo "hello";die;
//              if (!empty($_GET['oauth_verifier']) && !empty($this->session->userdata('access_token')) && !empty($this->session->userdata('access_token_secret'))) {
//           // We've got everything we need
//              $twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $this->session->userdata('access_token'), $this->session->userdata('access_token_secret'));
//       // Let's request the access token
//              $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
//       // Save it in a session var
//           $this->session->userdata('access_token') = $access_token;
//       // Let's get the user's info
//           $user_info = $twitteroauth->get('account/verify_credentials');
//       // Print user's info
// 
//           if (isset($user_info->error)) {
//               // Something's wrong, go back to square 1  
//               header('Location: login-twitter.php');
//           } else {
//           $data = array('uid'=>$user_info->id,
//                     'name'=>$user_info->name,
//                     'follower'=>'',
// 
//           );
// 
//                 $this->model_userlogintw->insertTwitterData($data);
//           }
//       }  
//    }
        
        
}