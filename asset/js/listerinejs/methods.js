var $ = jQuery.noConflict();
$(document).ready(function(){

	// CLOSE-POPUP
    /**
     * Masonry
     */
    // $(".contents").masonry({
    //     columnWidth: 256,
    //     itemSelector: '.content',
    //     "gutter": 0
    // });


// if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) ) {
//     $('body').parent().addClass("this-is-ipad");
// });


var isiPad = navigator.userAgent.match(/iPad/i) != null;
var isiPhone = navigator.userAgent.match(/iPhone/i) != null;
var isiPod = navigator.userAgent.match(/iPod/i) != null;


if(isiPad){
    $('body').parent().addClass("this-is-ipad");
};
if(isiPhone){
    $('body').parent().addClass("this-is-ipad");
};
if(isiPod){
    $('body').parent().addClass("this-is-ipad");
};


    $('.liked').click(function(event) {
        $(this).addClass('hearted');
    });
    $("#hukum, #contact, .term, .faq-box").mCustomScrollbar({
    });

    $('.top-content, .header-pop').append('<span class="close"></span>');
    $('.close').click(function(event) {
        $('.pop-up-wrapper').fadeOut(400); 
    	$('.pop-up-wrapper').html();
    });

    $('.intro-left').append('<a href="#" class="mobile"><span></span></a>');
    	$('.mobile').click(function(event) {
    		$(this).toggleClass('toggle');
    		$('.site-navigation, .faq').fadeToggle('300');
    	});
    $('.content:first-child, .content:nth-of-type(2)').append('<div class="new">NEW</div>');
	var panjang = $(document).width();
		if (panjang < 768) {
			$('.faq').append('<span class="help">help</span>');
		};

    $('.contents').waypoint(function() {
       $(".content").addClass('scale1');
    }, { offset: 'bottom-in-view' });


    var tinggi = $(".site-main").height();
    $(".pop-up-wrapper").css('height',tinggi+'px');
	
    $('.sample').click(function(event){
        event.preventDefault();
        window.history.pushState("string", "Form Sample", "?sample");
    	sample_content = '<div class="popup-info">'+
                '<div class="header-pop">'+
                    '<h3>product trial</h3>'+
                '</div>'+
                '<div class="req-sample">'+
                    '<span id="note" style="text-align: center; width: 100%; display: block; padding-top: 20px; color: red;" ></span>'+
                    '<form method="post" action="'+methods_var.form_url+'/index.php" id="sample">'+
                        '<label for="nama">nama lengkap*</label>'+
                            '<input type="text" id="name" name="nama">'+
                        '<label for="alamat" id="alamat">alamat*</label>'+
                            '<textarea name="alamat" id="alamat" class="alamat"></textarea>'+
                        '<label for="kota">kota*</label>'+
                            '<select name="kota" id="kota">'+
                                '<option value="jakarta">Jakarta</option>'+
                                '<option value="bogor">Bogor</option>'+
                                '<option value="depok">Depok</option>'+
                                '<option value="tangerang">Tangerang</option>'+
                                '<option value="bekasi">Bekasi</option>'+
                                '<option value="bandung">Bandung</option>'+
                                '<option value="surabaya">Surabaya</option>'+
                                '<option value="medan">Medan</option>'+
                            '</select>'+
                        '<label for="telepon">no. telepon*</label>'+
                            '<input type="text" id="tlp" name="telepon">'+
                        '<input type="hidden" name="post_type" id="post_type" value="sample" />'+
                        '<input type="hidden" name="action" value="post" />'+
                    '</form>'+
                   '<div class="vote">'+
                        '<h4>Apakah kamu sudah menggunakan Listerine?</h4>'+
                        '<input type="radio" checked="" value="one" id="r12" name="item2">'+
                            '<label class="radio circle" for="r12">'+
                                '<span class="big">'+
                                    '<span class="small"></span>'+
                                '</span>Ya'+
                            '</label>'+
                        '<input type="radio" checked="" value="two" id="r22" name="item2">'+
                            '<label class="radio circle" for="r22">'+
                                '<span class="big">'+
                                    '<span class="small"></span>'+
                                '</span>Belum'+
                            '</label>'+
                        '<input type="checkbox" id="c1" name="setuju">'+
                        '<label for="c1" id="check"><span></span>Saya bersedia dihubungi oleh Tim LISTERINE® di kemudian hari</label>'+
                        '<span class="note">*Khusus untuk wilayah Jabodetabek, Bandung, Surabaya, dan Medan</span>'+
                        '<input type="submit" value="SUBMIT" id="request_submit">'+
                    '</div>'+
                '</div>'+
            '</div>';
    	$('.pop-up-wrapper').html(sample_content);
    	$('.pop-up-wrapper').css('display', 'block');
    	$('.top-content, .header-pop').append('<span class="close"></span>');
   		closePopUp();
        submit_request_sample();
    });

    function submit_request_sample() {
        thanks = '<div class="popup-info">'+
                '<div class="header-pop">'+
                    '<h3>Terima Kasih</h3>'+
                '<span class="close"></span></div>'+
                '<div class="thankyou">'+
                    '<p>Tim LISTERINE® Indonesia akan langsung mengirimkan produk LISTERINE® Mouthwash ke alamat kamu, jika kamu termasuk ke dalam 1000 orang pendaftar pertama yang beruntung</p>'+
                    '<a href="#" class="ys-next-btn" onclick="jQuery(\'.pop-up-wrapper\').hide()"></a>'+
                '</div>'+
            '</div>';

        fail = '<div class="popup-info">'+
                '<div class="header-pop">'+
                    '<h3>product trial</h3>'+
                '<span class="close"></span></div>'+
                '<div class="thankyou">'+
                    '<p>Telah terjadi kesalahan, silahkan ulangi</p>'+
                '</div>'+
            '</div>';
        $('#request_submit').click(function(){
            nama = $('#name').val();
            alamat = $('.alamat').val();
            kota = $('#kota').val();
            telepon = $('#tlp').val();
            vote = $(".vote input[type='radio']:checked").val();
            if( '' == nama || '' == alamat || '' == kota || '' == telepon || '' == vote ) {
                $( '#note' ).html('Silahkan lengkapi data');
            }
            else if($("#c1").is(':checked')) {
                $.ajax({
                    type: "post",
                    url: methods_var.url,
                    data:{ nama: nama, alamat: alamat, kota: kota, telepon: telepon, vote: vote, action: 'submit_request_sample', nonce: methods_var.nonce },
                    dataType: 'html',
                    success: function(status) {
                        if('success' == status) {

                            $('.pop-up-wrapper').html(thanks);
                            $('.pop-up-wrapper').css('display', 'block');
                        }
                        else {

                            $('.pop-up-wrapper').html(fail);
                            $('.pop-up-wrapper').css('display', 'block');

                        }
                        $('.close').click(function(event) {
                            $('.pop-up-wrapper').fadeOut(400);
                        });
                    }
                });
            }
            else {
                $( '#note' ).html('Silahkan setujui persyaratan');
            }
        });
    }

    function closePopUp(){

        $('.close').click(function(event) {
            $('.pop-up-wrapper').fadeOut(400);
            $(".pop-up-wrapper").html('');
        });
        $('.pop-up-wrapper').click(function() {
            $('.close').click();
        });

        $('.popup-info, .popup-content').click(function(event){
            event.stopPropagation();
        });
        $('.pop-up-wrapper').click(function(e) {
          e.stopPropagation();
        });

    }

});