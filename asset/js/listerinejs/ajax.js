jQuery(document).ready(function() {

	like();
	load_single();
	share_fb();
	
	function closePopUp(){

        $('.close').click(function(event) {
            $('.pop-up-wrapper').fadeOut(400);
            $(".pop-up-wrapper").html('');
        });
        $('.pop-up-wrapper').click(function() {
            $('.close').click();
        });

        $('.popup-info, .popup-content').click(function(event){
            event.stopPropagation();
        });
        $('.pop-up-wrapper').click(function(e) {
          e.stopPropagation();
        });

    }

	function like(o) {
		if (!o)
			o = $(".liked");
		$(this).addClass('hearted');

		$(o).click(function(){
		
			heart = $(this);
			html = heart.html();
		
			// Retrieve post ID from data attribute
			post_id = heart.data("post_id");
			heart.html("<em>wait...</em>");
			// Ajax call
			$.ajax({
				type: "post",
				url: ajax_var.url,
				data: "action=post-like&nonce="+ajax_var.nonce+"&post_like=&post_id="+post_id,
				success: function(count){
					// If vote successful
					if(count != "already")
					{
						heart.html(count);
					} else {
						heart.html(html);
					}
				}
			});
			
			return false;
		});
	}

	function load_single(post_id_url) {
		$("figcaption").click( function( event ) {

			event.preventDefault();
			post_id = ( post_id_url ) ? post_id_url : $(this).data("post_id");
			extra_param = ( post_id_url ) ? '&disable=nav' : '';
			current_item_number = $(this).data("item_number");
			next_post_id = ( $('#item-'+(current_item_number+1) ).hasClass('not-active') ) ? -1: $('#item-'+(current_item_number+1) ).data("post_id");
			prev_post_id = ( $('#item-'+(current_item_number-1) ).hasClass('not-active') ) ? -1: $('#item-'+(current_item_number-1) ).data("post_id");
			window.history.pushState("string", "Testimonial", "?p="+post_id);
			$.ajax({
				type: "post",
				url: ajax_var.url,
				data: "action=load-post&nonce="+ajax_var.nonce+"&next="+next_post_id+"&prev="+prev_post_id+"&load_post=&post_id="+post_id+extra_param,
				success: function(content){
					$("body").scrollTop(0);
					content = jQuery(content);
					$(".pop-up-wrapper").html(content);
					$(".pop-up-wrapper").css('display', 'block');
				    $('.top-content').append('<span class="close"></span>');
				    content.find('.close').click(function(event) {
				    	$('.pop-up-wrapper').fadeOut(400);
						$(".pop-up-wrapper").html('');
						window.history.pushState("string", "Close", "");
				    });
				    like(content.find('.liked'));

				    $('.prev').click(function() {
						$('#'+prev_post_id).click();
				    });

				    $('.next').click(function() {
						$('#'+next_post_id).click();
				    });

	    			share_fb();

				    $('.pop-up-wrapper').click(function() {
				        $('.close').click();

				    });

				    $('.popup-info, .popup-content').click(function(event){
				        event.stopPropagation();
				    });
				    $('.pop-up-wrapper').click(function(e) {
				      e.stopPropagation();
				    });

					$(".footer-background li a").click(function(event){
						event.preventDefault();
						page = $(this).attr('class');
						window.history.pushState("string", "Form "+page, "?"+page);
						
						$.ajax({
							type: "post",
							url: ajax_var.url,
							data: "action=load_page&nonce="+ajax_var.nonce+"&load_page=&page_id="+page,
							success: function(content){

								$("body").scrollTop(0);
								$(".pop-up-wrapper").html(content);
								$(".pop-up-wrapper").css('display', 'block');
							    $('.header-pop').append('<span class="close"></span>');
							    $('.close').click(function(event) {
							    	$('.pop-up-wrapper').fadeOut(400);
									$(".pop-up-wrapper").html('');
							    });
							    var tinggi = $(".site-main").height();
				    			$(".pop-up-wrapper").css('height',tinggi+'px');

				    			$("#hukum, #contact, .term").mCustomScrollbar({
							        scrollButtons:{
							            enable:true
							        }
							    });
							    closePopUp();
							}
						});

					});

				}
			});
		});
	}



	$(window).scroll( function() {
		if($(window).scrollTop() == $(document).height() - $(window).height()) {
			load_post_list();
		}
	});

	$(".footer-left li a").click(function(event){
		event.preventDefault();
		page = $(this).attr('class');
		window.history.pushState("string", "Form "+page, "?"+page);

		$.ajax({
			type: "post",
			url: ajax_var.url,
			data: "action=load_page&nonce="+ajax_var.nonce+"&load_page=&page_id="+page,
			success: function(content){
				$("body").scrollTop(0);
				$(".pop-up-wrapper").html(content);
				$(".pop-up-wrapper").css('display', 'block');
			    $('.header-pop').append('<span class="close"></span>');
			    $('.close').click(function(event) {
			    	$('.pop-up-wrapper').fadeOut(400);
					$(".pop-up-wrapper").html('');
			    });
			    var tinggi = $(".site-main").height();
    			$(".pop-up-wrapper").css('height',tinggi+'px');
    			$("#hukum, #contact, .term").mCustomScrollbar({
			        scrollButtons:{
			            enable:true
			        }
			    });
			    closePopUp();
			}
		});
	});

	var isLoading = false;
	function load_post_list() {
		page_num = $('#main').data('page_num');
		page_num_inactive = $('#main').data('page_num_inactive');

		if(isLoading) {
			return false;
		}
		isLoading = true;
		$.ajax({
			type: "post",
			url: ajax_var.url,
			data: "action=next-page&nonce="+ajax_var.nonce+"&next_page=&page_num="+page_num+"&page_num_inactive="+page_num_inactive,
			beforeSend: function() {$(".loading").html('<h3>More freshness is coming...</h3>');},
			success: function(content){
				isLoading = false;
				$(".main-content ul").append(content);
				$(".loading").html(' ');
				$('#main').data('page_num', page_num+1);
			    like();
			    load_single();
			    share_fb();
			    var tinggi = $(".site-main").height();
    			$(".pop-up-wrapper").css('height',tinggi+'px');
    			dynamic_content();
			},
			error: function(){
				isLoading = false;
			},
			timeout: function(){
				isLoading = false;
			}
		});

	}


    function share_fb() {
	    $('.fb').click(function(event){
	    	event.preventDefault();
	        FB.ui(
	        {
	            method: 'feed',
	            name: 'Listerine Indonesia',
	            link: $(this).data('url'),
                picture: $('.pic-'+$(this).data('id')+' img').attr('src'),
	            caption: $(this).data('tagline'),
	            description: $(this).data('content')
	        },
	        function(response) {
	          if (response && response.post_id) {
	              // alert('Post was published.');
	          } else {
	              // alert('Post was not published.');
	          }
	        }
	        );
	    });

    }

})
